package com.irreel.tournoi.controller;

import com.irreel.tournoi.entity.Player;
import com.irreel.tournoi.exception.ResourceNotFoundException;
import com.irreel.tournoi.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PlayerController {

    @Autowired
    PlayerRepository playerRepository;

    @GetMapping("/player")
    public List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    @PostMapping("/player")
    public Player createPlayer(@Valid @RequestBody Player player) {
        return playerRepository.save(player);
    }

    @GetMapping("/player/{id}")
    public Player getPlayerById(@PathVariable(value = "id") Integer playerId) {
        return playerRepository.findById(playerId)
                .orElseThrow(() -> new ResourceNotFoundException("Player", "id", playerId));
    }

    @PutMapping("/player/{id}")
    public Player updatePlayer(@PathVariable(value = "id") Integer playerId,
                             @Valid @RequestBody Player playerDetails) {
        Player player = playerRepository.findById(playerId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", playerId));
        player.setName(playerDetails.getName());
        player.setLastGame(playerDetails.getLastGame());
        return playerRepository.save(player);
    }

    @DeleteMapping("/player/{id}")
    public ResponseEntity<?> deletePlayer(@PathVariable(value = "id") Integer playerId) {
        Player player = playerRepository.findById(playerId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", playerId));
        playerRepository.delete(player);

        return ResponseEntity.ok().build();
    }

}
